// Generated code from Butter Knife. Do not modify!
package googlemapsdemo.kmg.com.assignment.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import googlemapsdemo.kmg.com.assignment.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductDetails_ViewBinding implements Unbinder {
  private ProductDetails target;

  @UiThread
  public ProductDetails_ViewBinding(ProductDetails target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProductDetails_ViewBinding(ProductDetails target, View source) {
    this.target = target;

    target.selectImg = Utils.findRequiredViewAsType(source, R.id.selectImg, "field 'selectImg'", ImageView.class);
    target.productImg = Utils.findRequiredViewAsType(source, R.id.productImg, "field 'productImg'", ImageView.class);
    target.productNameTv = Utils.findRequiredViewAsType(source, R.id.productNameTv, "field 'productNameTv'", TextView.class);
    target.weightTv = Utils.findRequiredViewAsType(source, R.id.weightTv, "field 'weightTv'", TextView.class);
    target.priceTv = Utils.findRequiredViewAsType(source, R.id.priceTv, "field 'priceTv'", TextView.class);
    target.abtProduct = Utils.findRequiredViewAsType(source, R.id.abtProduct, "field 'abtProduct'", TextView.class);
    target.productAbout = Utils.findRequiredViewAsType(source, R.id.productAbout, "field 'productAbout'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductDetails target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.selectImg = null;
    target.productImg = null;
    target.productNameTv = null;
    target.weightTv = null;
    target.priceTv = null;
    target.abtProduct = null;
    target.productAbout = null;
  }
}
