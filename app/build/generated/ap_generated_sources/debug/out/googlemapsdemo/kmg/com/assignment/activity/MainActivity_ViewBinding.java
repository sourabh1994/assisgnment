// Generated code from Butter Knife. Do not modify!
package googlemapsdemo.kmg.com.assignment.activity;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import googlemapsdemo.kmg.com.assignment.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.compartment1 = Utils.findRequiredViewAsType(source, R.id.compartment1, "field 'compartment1'", RecyclerView.class);
    target.compartment2 = Utils.findRequiredViewAsType(source, R.id.compartment2, "field 'compartment2'", RecyclerView.class);
    target.compartment3 = Utils.findRequiredViewAsType(source, R.id.compartment3, "field 'compartment3'", RecyclerView.class);
    target.compartment4 = Utils.findRequiredViewAsType(source, R.id.compartment4, "field 'compartment4'", RecyclerView.class);
    target.racknoTv = Utils.findRequiredViewAsType(source, R.id.racknoTv, "field 'racknoTv'", TextView.class);
    target.rack2 = Utils.findRequiredViewAsType(source, R.id.rack2, "field 'rack2'", TextView.class);
    target.rack3 = Utils.findRequiredViewAsType(source, R.id.rack3, "field 'rack3'", TextView.class);
    target.rack4 = Utils.findRequiredViewAsType(source, R.id.rack4, "field 'rack4'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.compartment1 = null;
    target.compartment2 = null;
    target.compartment3 = null;
    target.compartment4 = null;
    target.racknoTv = null;
    target.rack2 = null;
    target.rack3 = null;
    target.rack4 = null;
  }
}
