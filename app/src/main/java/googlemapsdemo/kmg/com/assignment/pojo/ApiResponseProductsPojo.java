package googlemapsdemo.kmg.com.assignment.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiResponseProductsPojo {

    @SerializedName("pid")
    @Expose
    private long pid;

    @SerializedName("pname")
    @Expose
    private String pname;

    @SerializedName("pcode")
    @Expose
    private String pcode;

    @SerializedName("pdetails")
    @Expose
    private String pdetails;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("cost")
    @Expose
    private double cost;

    @SerializedName("weightgm")
    @Expose
    private String weightgm;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("amount")
    @Expose
    private long amount;

    @SerializedName("discount")
    @Expose
    private int discount;

    @SerializedName("total")
    @Expose
    private long total;

    @SerializedName("isVeg")
    @Expose
    private int isVeg;

    @SerializedName("qty")
    @Expose
    private long qty;

    @SerializedName("binIndex")
    @Expose
    private int binIndex;

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getPdetails() {
        return pdetails;
    }

    public void setPdetails(String pdetails) {
        this.pdetails = pdetails;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getWeightgm() {
        return weightgm;
    }

    public void setWeightgm(String weightgm) {
        this.weightgm = weightgm;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int isVeg() {
        return isVeg;
    }

    public void setVeg(int veg) {
        isVeg = veg;
    }

    public long getQty() {
        return qty;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    public int getBinIndex() {
        return binIndex;
    }

    public void setBinIndex(int binIndex) {
        this.binIndex = binIndex;
    }

    @Override
    public String toString() {
        return "{\n" +
                "        \"pid\": " + pid + ",\n" +
                "        \"pname\": \"" + pname + "\",\n" +
                "        \"pcode\": \"" + pcode + "\",\n" +
                "        \"pdetails\": \"" + pdetails + "\",\n" +
                "        \"photo\": \"" + photo + "\",\n" +
                "        \"cost\": " + cost + ",\n" +
                "        \"weightgm\": \"" + weightgm + "\",\n" +
                "        \"category\": \"" + category + "\",\n" +
                "        \"amount\": " + amount + ",\n" +
                "        \"discount\": " + discount + ",\n" +
                "        \"total\": " + total + ",\n" +
                "        \"isVeg\": " + isVeg + ",\n" +
                "        \"qty\": " + qty + ",\n" +
                "        \"binIndex\": " + binIndex + "\n" +
                "}";

    }
}
