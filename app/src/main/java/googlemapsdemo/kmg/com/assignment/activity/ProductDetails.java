package googlemapsdemo.kmg.com.assignment.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import googlemapsdemo.kmg.com.assignment.R;
import googlemapsdemo.kmg.com.assignment.pojo.ApiResponseProductsPojo;
import googlemapsdemo.kmg.com.assignment.utils.Util;

public class ProductDetails extends AppCompatActivity {

    @BindView(R.id.selectImg)
    ImageView selectImg;
    @BindView(R.id.productImg)
    ImageView productImg;
    @BindView(R.id.productNameTv)
    TextView productNameTv;
    @BindView(R.id.weightTv)
    TextView weightTv;
    @BindView(R.id.priceTv)
    TextView priceTv;
    @BindView(R.id.abtProduct)
    TextView abtProduct;
    @BindView(R.id.productAbout)
    TextView productAbout;
    Bundle bundle;
    Toolbar mToolbar;
    ApiResponseProductsPojo apiResponseProductsPojo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        bundle = getIntent().getExtras();
        apiResponseProductsPojo = new Gson().fromJson(bundle.getString("productData"), ApiResponseProductsPojo.class);
        getSupportActionBar().setTitle(apiResponseProductsPojo.getPname());
        if(apiResponseProductsPojo.getPhoto() != "")
            Util.invalidatePicasso(apiResponseProductsPojo.getPhoto());
            Picasso.get().load(apiResponseProductsPojo.getPhoto()).centerCrop().fit()
                    .networkPolicy(NetworkPolicy.NO_CACHE).noFade()
                    .into(productImg);
        if (apiResponseProductsPojo.getPname().length()>=23)
            productNameTv.setText(apiResponseProductsPojo.getPname().substring(0,23)+"...");
        else
            productNameTv.setText(apiResponseProductsPojo.getPname());
        weightTv.setText(apiResponseProductsPojo.getWeightgm()+" "+getString(R.string.gm_text));
        priceTv.setText("₹ "+String.valueOf(apiResponseProductsPojo.getCost()));
        productAbout.setText(apiResponseProductsPojo.getPname());

    }
}
