package googlemapsdemo.kmg.com.assignment.utils;

import android.app.Activity;
import android.app.ProgressDialog;

import com.squareup.picasso.Picasso;

public class Util {
    private static ProgressDialog dialog;
    public static void showProgressDialogue(Activity activity, String title, String message) {
        if (dialog != null && dialog.isShowing()) {
            try {
//                dialog.dismiss();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        dialog = new ProgressDialog(activity);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        if (!(activity).isFinishing())
            dialog.show();
    }

    public static void dismissProgressDialogue() {
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public static void invalidatePicasso(String strUrl) {
        try {
            Picasso.get().invalidate(strUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
