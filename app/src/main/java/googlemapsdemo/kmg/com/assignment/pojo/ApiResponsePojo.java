package googlemapsdemo.kmg.com.assignment.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiResponsePojo {

    @SerializedName("Message")
    @Expose
    private String Message;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("kioskid")
    @Expose
    private String kioskid;

    @SerializedName("compartments")
    @Expose
    private List<ApiResponseCompartmentPojo> compartments;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKioskid() {
        return kioskid;
    }

    public void setKioskid(String kioskid) {
        this.kioskid = kioskid;
    }

    public List<ApiResponseCompartmentPojo> getCompartments() {
        return compartments;
    }

    public void setCompartments(List<ApiResponseCompartmentPojo> compartments) {
        this.compartments = compartments;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"Message\": \"" + Message + "\",\n" +
                "  \"status\": \"" + status + "\",\n" +
                "  \"kioskid\": \"" + kioskid + "\",\n" +
                "  \"compartments\": " + compartments + "\n" +
                "}";
    }

}
