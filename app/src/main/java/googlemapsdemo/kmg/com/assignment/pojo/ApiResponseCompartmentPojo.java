package googlemapsdemo.kmg.com.assignment.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiResponseCompartmentPojo {
    @SerializedName("compartmentid")
    @Expose
    private long compartmentid;

    @SerializedName("products")
    @Expose
    private List<ApiResponseProductsPojo> products;

    public long getCompartmentid() {
        return compartmentid;
    }

    public void setCompartmentid(long compartmentid) {
        this.compartmentid = compartmentid;
    }

    public List<ApiResponseProductsPojo> getProducts() {
        return products;
    }

    public void setProducts(List<ApiResponseProductsPojo> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"compartmentid\": " + compartmentid + ",\n" +
                "  \"products\": " + products + "\n" +
                "}";
    }
}
