package googlemapsdemo.kmg.com.assignment.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import googlemapsdemo.kmg.com.assignment.R;
import googlemapsdemo.kmg.com.assignment.activity.ProductDetails;
import googlemapsdemo.kmg.com.assignment.pojo.ApiResponseCompartmentPojo;
import googlemapsdemo.kmg.com.assignment.pojo.ApiResponseProductsPojo;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewHolder> {

    List<ApiResponseProductsPojo> apiResponseProductsPojos;
    Context context;

    public DataAdapter(List<ApiResponseProductsPojo> apiResponseProductsPojos, Context context) {
        this.apiResponseProductsPojos = apiResponseProductsPojos;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.compartment_content, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        ApiResponseProductsPojo apiResponseProductsPojo=apiResponseProductsPojos.get(i);
        if(apiResponseProductsPojo.getPhoto() != "")
        Picasso.get().load(apiResponseProductsPojo.getPhoto()).centerCrop().fit()
                .networkPolicy(NetworkPolicy.NO_CACHE).noFade()
                .into(myViewHolder.productImg);
        myViewHolder.qtyLeftTv.setText(apiResponseProductsPojo.getQty()+" "+context.getString(R.string.left_text));
        if (apiResponseProductsPojo.getPname().length()>=23)
            myViewHolder.productNameTv.setText(apiResponseProductsPojo.getPname().substring(0,23)+"...");
        else
            myViewHolder.productNameTv.setText(apiResponseProductsPojo.getPname());
        myViewHolder.productWeightTv.setText(apiResponseProductsPojo.getWeightgm()+" "+context.getString(R.string.gm_text));
        myViewHolder.priceTv.setText("₹ "+String.valueOf(apiResponseProductsPojo.getCost()));

        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ProductDetails.class);
                Bundle bundle=new Bundle();
                bundle.putString("productData",apiResponseProductsPojos.get(i).toString());
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return apiResponseProductsPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView qtyLeftTv,productNameTv,productWeightTv,priceTv;
        ImageView productImg;
        Button buyBtn;
        CardView cardView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            qtyLeftTv=itemView.findViewById(R.id.qtyLeftTv);
            priceTv=itemView.findViewById(R.id.priceTv);
            productNameTv=itemView.findViewById(R.id.productNameTv);
            productWeightTv=itemView.findViewById(R.id.weightTv);
            productImg=itemView.findViewById(R.id.productImg);
            buyBtn=itemView.findViewById(R.id.buyBtn);
            cardView=itemView.findViewById(R.id.CardView);
        }
    }
}