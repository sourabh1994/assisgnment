package googlemapsdemo.kmg.com.assignment.retrofit;


import googlemapsdemo.kmg.com.assignment.pojo.ApiResponsePojo;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

   @GET(AppUri.getProducts)
   Call<ApiResponsePojo> getProductDetails(  );

   }