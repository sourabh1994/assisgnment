package googlemapsdemo.kmg.com.assignment.activity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import googlemapsdemo.kmg.com.assignment.R;
import googlemapsdemo.kmg.com.assignment.adapter.DataAdapter;
import googlemapsdemo.kmg.com.assignment.pojo.ApiResponsePojo;
import googlemapsdemo.kmg.com.assignment.pojo.ApiResponseProductsPojo;
import googlemapsdemo.kmg.com.assignment.retrofit.ApiClient;
import googlemapsdemo.kmg.com.assignment.retrofit.ApiInterface;
import googlemapsdemo.kmg.com.assignment.utils.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.compartment1)
    RecyclerView compartment1;
    @BindView(R.id.compartment2)
    RecyclerView compartment2;
    @BindView(R.id.compartment3)
    RecyclerView compartment3;
    @BindView(R.id.compartment4)
    RecyclerView compartment4;
    DataAdapter FCompartment;
    DataAdapter SCompartment;
    DataAdapter TCompartment;
    DataAdapter FoCompartment;
    List<ApiResponseProductsPojo> FCompartmentData = new ArrayList<>();
    List<ApiResponseProductsPojo> SCompartmentData = new ArrayList<>();
    List<ApiResponseProductsPojo> TCompartmentData = new ArrayList<>();
    List<ApiResponseProductsPojo> FoCompartmentData = new ArrayList<>();
    @BindView(R.id.racknoTv)
    TextView racknoTv;
    @BindView(R.id.rack2)
    TextView rack2;
    @BindView(R.id.rack3)
    TextView rack3;
    @BindView(R.id.rack4)
    TextView rack4;
    Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView.LayoutManager mLayoutManagerS = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView.LayoutManager mLayoutManagerT = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView.LayoutManager mLayoutManagerF = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        FCompartment = new DataAdapter(FCompartmentData, this);
        SCompartment = new DataAdapter(SCompartmentData, this);
        TCompartment = new DataAdapter(TCompartmentData, this);
        FoCompartment = new DataAdapter(FoCompartmentData, this);

        compartment1.setLayoutManager(mLayoutManager);
        compartment1.setItemAnimator(new DefaultItemAnimator());
        compartment1.setAdapter(FCompartment);
        compartment1.clearOnChildAttachStateChangeListeners();

        compartment2.setLayoutManager(mLayoutManagerS);
        compartment2.setItemAnimator(new DefaultItemAnimator());
        compartment2.setAdapter(SCompartment);
        compartment2.clearOnChildAttachStateChangeListeners();

        compartment3.setLayoutManager(mLayoutManagerT);
        compartment3.setItemAnimator(new DefaultItemAnimator());
        compartment3.setAdapter(TCompartment);
        compartment3.clearOnChildAttachStateChangeListeners();

        compartment4.setLayoutManager(mLayoutManagerF);
        compartment4.setItemAnimator(new DefaultItemAnimator());
        compartment4.setAdapter(FoCompartment);
        compartment4.clearOnChildAttachStateChangeListeners();

        getDatatFromApi();

    }

    private void getDatatFromApi() {
        Util.showProgressDialogue(this, "", getString(R.string.loading_text));
        ApiInterface apiInterface = ApiClient.getClient("").create(ApiInterface.class);
        Call<ApiResponsePojo> call = apiInterface.getProductDetails();
        call.enqueue(new Callback<ApiResponsePojo>() {
            @Override
            public void onResponse(Call<ApiResponsePojo> call, Response<ApiResponsePojo> response) {
                Util.dismissProgressDialogue();
                ApiResponsePojo apiResponsePojo = response.body();
                if (response.isSuccessful()) {
                    if (apiResponsePojo.getCompartments() != null && apiResponsePojo.getCompartments().size() > 0) {
                        getSupportActionBar().setTitle(apiResponsePojo.getKioskid());
                        FCompartmentData.clear();
                        SCompartmentData.clear();
                        TCompartmentData.clear();
                        FoCompartmentData.clear();

                        for (int i = 0; i < apiResponsePojo.getCompartments().size(); i++) {
                            if (apiResponsePojo.getCompartments().get(i).getCompartmentid() == 01) {
                                racknoTv.setText("Rack"+" "+"01"
                                        +" - "+apiResponsePojo.getCompartments().get(i).getProducts().size()+" "+"items");
                                for (int j = 0; j < apiResponsePojo.getCompartments().get(i).getProducts().size(); j++)
                                    FCompartmentData.add(apiResponsePojo.getCompartments().get(i).getProducts().get(j));
                            } else if (apiResponsePojo.getCompartments().get(i).getCompartmentid() == 02) {
                                rack2.setText("Rack"+" "+"02"
                                        +" - "+apiResponsePojo.getCompartments().get(i).getProducts().size()+" "+"items");
                                for (int j = 0; j < apiResponsePojo.getCompartments().get(i).getProducts().size(); j++)
                                    SCompartmentData.add(apiResponsePojo.getCompartments().get(i).getProducts().get(j));
                            } else if (apiResponsePojo.getCompartments().get(i).getCompartmentid() == 03) {
                                rack3.setText("Rack"+" "+"03"
                                        +" - "+apiResponsePojo.getCompartments().get(i).getProducts().size()+" "+"items");
                                for (int j = 0; j < apiResponsePojo.getCompartments().get(i).getProducts().size(); j++)
                                    TCompartmentData.add(apiResponsePojo.getCompartments().get(i).getProducts().get(j));
                            } else {
                                rack4.setText("Rack"+" "+"04"
                                        +" - "+apiResponsePojo.getCompartments().get(i).getProducts().size()+" "+"items");
                                for (int j = 0; j < apiResponsePojo.getCompartments().get(i).getProducts().size(); j++)
                                    FoCompartmentData.add(apiResponsePojo.getCompartments().get(i).getProducts().get(j));
                            }
                        }
                        FCompartment.notifyDataSetChanged();
                        SCompartment.notifyDataSetChanged();
                        TCompartment.notifyDataSetChanged();
                        FoCompartment.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponsePojo> call, Throwable t) {
                Util.dismissProgressDialogue();
                Toast.makeText(MainActivity.this, getString(R.string.check_internet), Toast.LENGTH_LONG).show();
            }
        });
    }
}
